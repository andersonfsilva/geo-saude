
/*
    MODEL LIRAA FORM
*/

const mongoose = require('mongoose')
var Schema = mongoose.Schema;



// Criando schema para o formulário Liraa.

var LiraaFormSchema = new Schema( {

    places: {
        type: String,
        required: [true, 'Informe o logradouro corretamente']       // Obriga que seja informado o endereço.
    },

    immobile: {
        tpImmobile: Number,
        dateVisit: String
    },

    vtAedes: {
        tpBreedingGrounds: {        // Tipo de criadouros.
          A1: Number,
          A2: Number,
          B: Number,
          C: Number,
          D1: Number,
          D2: Number,
          E: Number
        }
    },

    depositsRemoved: {
        type: Number
    },

    larvicides: {
        BTiG_gramas: Number,
        BTiG_depositos: Number,      
        BTiWDg_gramas: Number,
        BTiWDg_depositos: Number,
    }

})

module.exports = mongoose.model('LiraaForm', LiraaFormSchema)